from dialog_to_animation.parser import parse

def animate(source):
    '''
    Creates a cartoon scene for `source`
    '''
    characters, dialog = parse(source)
    # work on character images
    # animate them / speech
    return dialog
